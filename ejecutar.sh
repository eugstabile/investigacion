#!/bin/bash
#patron de acceso
access='rw'
#configurar layer
layer=$1
con="err"
if [ $layer == 'lesslays' ]
then
    con="lover"
elif [ $layer == 'morelays' ]
then
    con="mover"
elif [ $layer == 'otherfiles' ]
then
    con="big"
else
    echo "Error con la capa, utilizar otherfiles, morelays o lesslays"
    exit 0
fi
#configurar driver
driver=$2
drivercheck=$( docker info --format '{{json .}}' | grep  -oh $driver | head -1 )

#storageDriver () {
#    echo "cambiando driver, se eliminarán las imagenes"
#    read choose
#    sudo service docker stop
#    sudo dockerd -s $driver &
#    echo "reiniciando el servicio docker"
#    echo "Iniciando con driver $drivercheck e imagenes"
#    ./start.sh
#}

if [ $driver == $drivercheck ]
then
    echo "Se utilizará el driver $driver"
else
    echo "Error, modificar el driver en Docker"
    exit 0
fi
    

#funciones de ejecucion
readSmall () {
    echo "Iniciando read small"
    for size in 512 1K 10K; do
        tiempo=0.0
        veces=0
        start=1
        end=$1
        for ((i=start; i<=end; i++)); do
            #manejo de docker
            docker stop $con 
            docker rm $con
            docker run --name $con -dit -v "$(pwd)"/tests:/testing:shared $layer
            start_time=$( date +%s.%N )
            docker exec -it -e SZ=${size} $con bash -c 'sh /testing/read-small-files ${SZ}'
            elapsed_time=$( date +%s.%N --date="$start_time seconds ago" )
            tiempo="$(bc<<<"$tiempo + $elapsed_time")"
            veces=$(($veces + 1))
        done;
        total=$(echo "scale=2; $tiempo/$veces" | bc)
        echo "${size};${total};${veces};${layer};${driver};${access}" >> resultados/read_small
        cat resultados/read_small
    done;
} 

readSmallCont () {
    echo "Iniciando read small contenedor"
    for size in 512 1K 10K; do
        tiempo=0.0
        veces=0
        start=1
        end=$1
        for ((i=start; i<=end; i++)); do
            #manejo de docker
            docker stop $con 
            docker rm $con
            docker run --name $con -dit -v "$(pwd)"/tests:/testing:shared $layer
            echo "Creando dirs"
            docker exec -it $con bash -c 'sh /testing/prueba'
            start_time=$( date +%s.%N )
            docker exec -it -e SZ=${size} $con bash -c 'sh /testing/read-small-files_con ${SZ}'
            elapsed_time=$( date +%s.%N --date="$start_time seconds ago" )
            tiempo="$(bc<<<"$tiempo + $elapsed_time")"
            veces=$(($veces + 1))
        done;
        total=$(echo "scale=2; $tiempo/$veces" | bc)
        echo "${size};${total};${veces};${layer};${driver};${access}" >> resultados/read_small_con
        cat resultados/read_small_con
    done;
} 

appendSmall() {
    echo "Iniciando append small"
    for size in 512 1K 10K; do
        tiempo=0.0
        veces=0
        start=1
        end=$1
        for ((i=start; i<=end; i++)); do
            #manejo de docker
            docker stop $con 
            docker rm $con
            docker run --name $con -dit -v "$(pwd)"/tests:/testing:shared $layer
            echo "Creando dirs"
            docker exec -it $con bash -c 'sh /testing/prueba'
            start_time=$( date +%s.%N )
            docker exec -it -e SZ=${size} $con bash -c 'sh /testing/append-to-small-files ${SZ}'
            elapsed_time=$( date +%s.%N --date="$start_time seconds ago" )
            tiempo="$(bc<<<"$tiempo + $elapsed_time")"
            veces=$(($veces + 1))
        done;
        total=$(echo "scale=2; $tiempo/$veces" | bc)
        echo "${size};${total};${veces};${layer};${driver};${access}" >> resultados/append_small
        cat resultados/append_small
    done;
}

appendSmallCont () {
    echo "Iniciando append small contenedor"
    for size in 512 1K 10K; do
        tiempo=0.0
        veces=0
        start=1
        end=$1
        for ((i=start; i<=end; i++)); do
            #manejo de docker
            docker stop $con 
            docker rm $con
            docker run --name $con -dit -v "$(pwd)"/tests:/testing:shared $layer
            echo "Creando dirs"
            docker exec -it $con bash -c 'sh /testing/prueba'
            start_time=$( date +%s.%N )
            docker exec -it -e SZ=${size} $con bash -c 'sh /testing/append-to-small-files_con ${SZ}'
            elapsed_time=$( date +%s.%N --date="$start_time seconds ago" )
            tiempo="$(bc<<<"$tiempo + $elapsed_time")"
            veces=$(($veces + 1))
        done;
        total=$(echo "scale=2; $tiempo/$veces" | bc)
        echo "${size};${total};${veces};${layer};${driver};${access}" >> resultados/append_small_con
        cat resultados/append_small_con
    done;
} 

# Comprobaciones de patrones

rwSmall () {
    echo "patron inverso"
    for size in 1M 10M 100M; do
        tiempo=0.0
        veces=0
        start=1
        end=$1
        for ((i=start; i<=end; i++)); do
            #manejo de docker
            docker stop $con 
            docker rm $con
            docker run --name $con -dit -v "$(pwd)"/tests:/testing:shared $layer
            start_time=$( date +%s.%N )
            docker exec -it --env SZ=${size} --env AC=${access} $con bash -c 'sh /testing/big-files ${SZ} ${AC}'
            elapsed_time=$( date +%s.%N --date="$start_time seconds ago" )
            tiempo="$(bc<<<"$tiempo + $elapsed_time")"
            veces=$(($veces + 1))
        done;
        total=$(echo "scale=2; $tiempo/$veces" | bc)
        echo "${size};${total};${veces};${layer};${driver};${access}" >> resultados/bigf
        cat resultados/bigf
    done;
} 


# Ejecucion

principal () {
  readSmall 15 
  readSmallCont 15
  appendSmall 15 
  appendSmallCont 15 
}

patron_de_acceso_inverso () {
    echo "Patrón de acceso Escritura-Lectura (inverso)"
    rwSmall 15 
    access="wr" 
    rwSmall 15
}

principal
#patron_de_acceso_inverso
