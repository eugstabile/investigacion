!#/bin/bash 
docker stop lover && docker stop mover && docker stop big
docker rm lover && docker rm mover && docker rm big
docker rmi lesslays && docker rmi morelays && docker rmi otherfiles

echo "creando imagen con los directorios en las capas más cercanas al contenedor"
docker build -t lesslays "$(pwd)"/imagenes/lo/
#echo "creando imagen con los directorios en las capas más lejanas al contenedor"
docker build -t morelays "$(pwd)"/imagenes/mo/
echo "creando imagen con archivos grandes"
docker build -t otherfiles "$(pwd)"/imagenes/other/

echo "hecho, iniciando tests con overlay2"
./ejecutar.sh lesslays overlay2
#./ejecutar.sh morelays overlay2
#./ejecutar.sh otherfiles overlay2
